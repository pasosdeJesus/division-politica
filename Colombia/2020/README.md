* CRVeredas_2020.rar descargado el 21.Sep.2021 desde 
  http://geoportal.dane.gov.co/servicios/descarga-y-metadatos/descarga-nivel-de-referencia-de-veredas/ 
  eligiendo 2020 y en el enlace que decía SHP
  Fue publicado con licencia Creative Commons Atribución 4.0 Internacional según
  <https://web.archive.org/web/20191119221542/https://geoportal.dane.gov.co/acerca-del-geoportal/licencia-y-condiciones-de-uso/>
* Veredas_DANE_2020.csv se extrajo del anterior así: se descomprimió, se extrajo 
  CRVeredas_2020.dbf, y con LibreOffice Calc se guardo como CSV cambiando 
  nombre de los encabezados a los uniformes.

* DIVIPOLA_2020723.xls DIVIPOLA descargado el 24.Jul.2021 desde
  <https://geoportal.dane.gov.co/laboratorio/serviciosjson/gdivipola/servicios/reporte.php?fechaVigencia=2020-7-24>
  Fue publicado con licencia Creative Commons Atribución 4.0 Internacional según
  <https://web.archive.org/web/20191119221542/https://geoportal.dane.gov.co/acerca-del-geoportal/licencia-y-condiciones-de-uso/>

* Listados_DIVIPOLA (6).xlsx DIVIPOLA enviado por el DANE a Vladimir Támara el 10 de 
  Agosto de 2020 en respuesta a derecho de petción, ver
  <https://gitlab.com/pasosdeJesus/division-politica/-/tree/main/Colombia/2019/correcciones/respuesta_derecho_de_peticion_DIVIPOLA>
  Debe tener la misma licencia de otros DIVIPOLA del DANE, ie. 
  Creative Commons Atribución 4.0 Internacional según 
  <https://web.archive.org/web/20191119221542/https://geoportal.dane.gov.co/acerca-del-geoportal/licencia-y-condiciones-de-uso/>
* DIVIPOLA_DANE_2020_07.csv el anterior en CSV

* DIVIPOLA_Diciembre_2020.xlsx DIVIPOLA enviado por el DANE a Vladimir
  Támara el 5 de Enero de 2022 en respuesta a derecho de petición, ver
  <https://gitlab.com/pasosdeJesus/division-politica/-/tree/main/Colombia/2020/correcciones/respuesta_derecho_de_peticion_Dic_2021/20222450000051T_R_VTAMARA_04012022.pdf>

* SHP_CRVEREDAS_2020.zip Capa de referencia Veredas del DANE corregida, 
  descargada el 9.Feb.2022 de 
  <https://www.dane.gov.co/files/geoportal-provisional/index.html>
  Ubicación dada en respuesta a derecho de petición del 5 de Enero de 2022
  <https://gitlab.com/pasosdeJesus/division-politica/-/tree/main/Colombia/2020/correcciones/respuesta_derecho_de_peticion_Dic_2021/20222450000051T_R_VTAMARA_04012022.pdf>
* Veredas_DANE_2020-v2.csv se extrajo del anterior así: se descomprimió, se 
  extrajo CRVeredas_2020.dbf, y con LibreOffice Calc se guardo como CSV 
  cambiando nombre de los encabezados a los uniformes.


* veredas_de_colombia-shp.zip capa de veredas descargada el 10.Nov.2021 de
  https://data.humdata.org/dataset/veredas-de-colombia.   Su licencia
  según la sección Metadata de la misma página es Creative Commons 
  Attribution International. Es un conjunto de datos de 2015 actualizado el 
  2020.
* Veredas_OCHA_2020.csv el anterior en CSV (tras descomprimir y convertir
  el .dbf a .csv usando LibreOffice 7 y suponiendo que era UTF-8).

