class Bitprobcsv
  # Registro de problemas, indexado por vector fila y el valor es el 
  # vector [fila, cod_municipio, departamento, municipio, 
  # cod_vereda, vereda, vector problemas].

  def initialize
    @lprob = {}
    @maxprob = 0 # Máximo de problemas por fila
    @filamasprob = -1 # Fila con máximo de problemas
  end

  def fila_mas
    @filamasprob
  end

  def numreg
    @lprob.count
  end

  # Función para registrar un problema
  def reg_prob(fila, codmun, departamento, municipio, codver, vereda, problema)
#    if !defined?(@lprob)
#      @lprob = {}
#      @maxprob = 0 # Máximo de problemas por fila
#      @filamasprob = -1 # Fila con máximo de problemas
#    end
    if @lprob[fila]
      pa = @lprob[fila]
      if pa[0] != codmun || pa[1] != departamento || pa[2] != municipio || 
          pa[3] != codver || pa[4] != vereda
        STDERR.puts "Problema con segundo reporte para fila #{fila}"
        byebug
      end
      @lprob[fila] = [codmun, departamento, municipio, codver, vereda, 
                     pa[5] + [problema]]
      if @maxprob < pa[5].length + 1
        @maxprob = pa[5].length + 1
        @filamasprob = fila
      end
    else
      @lprob[fila] = [codmun, departamento, municipio, codver, vereda, [problema]]
    end
  end


  # Genera CSV de problemas
  def gen_csv_prob
    enc = ['fila', 'cod_municipio', 'departamento', 'municipio', 
           'cod_vereda', 'vereda']
    (1..@maxprob).each do |i|
      enc << "problema_"+i.to_s
    end
    puts enc.to_csv
    @lprob.each do |f, vprob|
      vr = [f, vprob[0], vprob[1], vprob[2], vprob[3], vprob[4]]
      (0..(@maxprob-1)).each do |i|
        vr << (i < vprob[5].length ? vprob[5][i] : '')
      end
      puts vr.to_csv
    end
  end

end

