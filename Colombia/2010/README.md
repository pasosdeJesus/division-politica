
* Listado_2010.xls  DIVIPOLA 2010 descargado el 18.Mar.2019 de
  https://geoportal.dane.gov.co/metadatos/historicos/archivos/Listado_2010.xls
  Publicado con licencia Creative Commons Atribución 4.0 Internacional según
  <https://web.archive.org/web/20191119221542/https://geoportal.dane.gov.co/acerca-del-geoportal/licencia-y-condiciones-de-uso/>
 
* DIVIPOLA_DANE_2010.csv el anterior convertido a CSV con Libreoffice.
