* R_VEREDAS.shp.zip descargado el 10.Nov.2021 de
  <https://geoportal.igac.gov.co/contenido/datos-abiertos-catastro>
  Según licencia Creative Commons CC-BY 4.0 la licencia 
  es Creative Commons CC-BY 4.0
* Veredas_IGAC_2021-11.csv se extrajo del anterior así: se descomprimió, 
  se extrajo CRVeredas_2017.dbf y con LibreOffice Calc se guardo como 
  CSV cambiando nombres de encabezados a los uniformes

* Listado_DIVIPOLA.zip DIVIPOLA 2021 descargado de
  https://www.dane.gov.co/files/geoportal-provisional/index.html el
  10.Feb.2022
