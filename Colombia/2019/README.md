
* Listado_2019.xlsx DIVIPOLA producido por DANE descargado en Jul.2020 de
  http://geoportal.dane.gov.co/descargas/metadatos/historicos/archivos/Listado_2019.xlsx
  Fue publicado con licencia Creative Commons Atribución 4.0 Internacional según
  <https://web.archive.org/web/20191119221542/https://geoportal.dane.gov.co/acerca-del-geoportal/licencia-y-condiciones-de-uso/>
